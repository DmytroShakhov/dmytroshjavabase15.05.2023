package com.shakhov;

public class ChineseDynasties {

    public static void main(String[] args) {

        int liWarrior = 13;
        int liArcher = 24;
        int liRider = 46;
        int warriors = 860;
        int attackLi = warriors * (liWarrior + liArcher + liRider);

        System.out.println("Attack rate Li = " + attackLi);

        int minWarrior = 9;
        int minArcher = 35;
        int minRider = 12;
        double a = 1.5; // кількість воїнів династії Мінь кожного типу в півтора рази більша
        int attackMin = (int) (warriors * a * (minWarrior + minArcher + minRider));

        System.out.println("Attack rate Min = " + attackMin);

        long totalAttack = attackLi + attackMin;
        System.out.println("Total attack rate Li and Min = " + totalAttack);


    }

}


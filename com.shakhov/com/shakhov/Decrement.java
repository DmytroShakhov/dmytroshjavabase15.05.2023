package com.shakhov;

public class Decrement {

    public static void main(String[] args) {

        int day = 6;

        day = 5 * day++;
        System.out.println("i = " + day);
        day = 10 * ++day;
        System.out.println("i = " + day);
    }
}
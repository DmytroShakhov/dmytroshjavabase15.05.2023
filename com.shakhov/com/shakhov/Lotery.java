package com.shakhov;

import java.util.Arrays;

public class Lotery {

    public static void main(String[] args) {

        int[] lotteryOrganizer = new int[7];
        int[] numbersLottery = getRandomNumbers(lotteryOrganizer);
        prinArray("Organizer of the lottery:", numbersLottery);
        Arrays.sort(numbersLottery);

        int[] playersLottery = new int[7];
        int[] numbersPlaeyr = getRandomNumbers(playersLottery);
        prinArray("Plaer of the lottery:    ", numbersPlaeyr);
        Arrays.sort(numbersPlaeyr);

        prinArray("Sort organizer of the lottery:", numbersLottery);
        prinArray("Sort plaer of the lottery:    ", numbersPlaeyr);

        System.out.println("Number of matches - " + res1(numbersLottery, numbersPlaeyr));
    }

    public static int[] getRandomNumbers(int[] numbers) {
        int min = 0;
        int max = 9;
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * ((max - min) + 1) + min);
        }
        return numbers;
    }

    public static void prinArray(String message, int[] arr) {
        System.out.println(message + Arrays.toString(arr));

    }

    public static int res1(int[] newOrganize, int[] plaeyr) {
        int result = 0;
        for (int i = 0; i < newOrganize.length; i++) {
            if (newOrganize[i] == plaeyr[i]) {
                result++;
            }
        }
        return result;
    }
}
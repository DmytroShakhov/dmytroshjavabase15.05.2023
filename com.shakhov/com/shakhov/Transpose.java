package com.shakhov;

import java.util.Scanner;

public class Transpose {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter matrix line M: ");
        int line = scanner.nextInt();
        System.out.print("Enter matrix column N: ");
        int colum = scanner.nextInt();
        int[][] matrix = new int[line][colum];
        int[][] transMatrix = new int[colum][line];

        int[][] newMatrix = fillMatrix(matrix);

        for (int i = 0; i < line; i++) {
            for (int k = 0; k < colum; k++) {
                System.out.print(matrix[i][k] + " ");
            }
            System.out.println();
        }
        System.out.println("Transposed matrix");
        for (int i = 0; i < colum; i++) {
            for (int k = 0; k < line; k++) {
                transMatrix[i][k] = newMatrix[k][i];
                System.out.print(transMatrix[i][k] + " ");
            }
            System.out.println("   ");
        }
    }
    static int[][] fillMatrix(int[][] tmpMat) {
        for (int i = 0; i < tmpMat.length; i++) {
            for (int j = 0; j < tmpMat[i].length; j++) {
                tmpMat[i][j] = (int) (Math.random() * 100);
            }
        }
        return tmpMat;
    }
}






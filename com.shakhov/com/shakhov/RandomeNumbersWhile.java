package com.shakhov;

import java.util.Arrays;

public class RandomeNumbersWhile {

    public static void main(String[] args) {

        int[] team1 = new int[25];
        int playersAgeMin = 18;
        int playersAgeMax = 40;
        for (int i = 0; i < team1.length; i++) {
            team1[i] = (int) (Math.random() * (playersAgeMax - playersAgeMin + 1)) + playersAgeMin;
        }
        System.out.println("Team #1" + " - age players: " + Arrays.toString(team1));


        int[] team2 = new int[25];
        for (int i = 0; i < team2.length; i++) {
            team2[i] = (int) (Math.random() * (playersAgeMax - playersAgeMin + 1)) + playersAgeMin;
        }
        System.out.println("Team #2" + " - age players: " + Arrays.toString(team2));

        int averageTeam1 = 0;
        if (team1.length > 0) {
            int tmp = 0;
            for (int j = 0; j < team1.length; j++) {
                tmp += team1[j];
            }
            averageTeam1 = tmp / team1.length;
            System.out.println("Team #1" + " - average age players: " + averageTeam1);


            int averageAgeTeam2 = 0;
            if (team2.length > 0) {
                int tmp2 = 0;
                for (int j = 0; j < team2.length; j++) {
                    tmp2 += team2[j];
                }
                averageAgeTeam2 = tmp2 / team2.length;


                System.out.println("Team #2" + " - average age players: " + averageAgeTeam2);
            }
        }
    }
}


//    int tmp2;
//  boolean statusSort = false;
//  int s = 0;
//  while (!statusSort) {
//      s++;
//      statusSort = true;

//   for (int k = 0; k < mas.length - 1; k++) {
//       if (mas[k] > mas[k + 1]) {
//           statusSort = false;
//          tmp2 = mas[k];
//          mas[k] = mas[k + 1];
//         mas[k + 1] = tmp2;


//     System.out.println(s + " - " + Arrays.toString(mas));
//  System.out.println("Cорт2 " + Arrays.toString(mas));

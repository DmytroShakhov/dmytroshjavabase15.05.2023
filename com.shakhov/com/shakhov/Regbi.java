package com.shakhov;


import java.util.Arrays;

public class Regbi {

    public static void main(String[] args) {

        int playersAgeMin = 18;
        int playersAgeMax = 40;

        int[] numberPlayersTeam1 = new int[25];
        for (int i = 0; i < numberPlayersTeam1.length; i++) {
            numberPlayersTeam1[i] = (int) (Math.random() * (playersAgeMax - playersAgeMin + 1)) + playersAgeMin;
        }
        System.out.println("Team #1" + " - age players: " + Arrays.toString(numberPlayersTeam1));

        int[] numberPlayersTeam2 = new int[25];
        for (int i = 0; i < numberPlayersTeam2.length; i++) {
            numberPlayersTeam2[i] = (int) (Math.random() * (playersAgeMax - playersAgeMin + 1)) + playersAgeMin;
        }
        System.out.println("Team #2" + " - age players: " + Arrays.toString(numberPlayersTeam2));

        int averageAgeTeam1;
        int sum1 = 0;
        for (int i : numberPlayersTeam1) {
            sum1 += i;
        }
        averageAgeTeam1 = sum1 / numberPlayersTeam1.length;
        System.out.println("Team #1" + " - average age players: " + averageAgeTeam1);

        int averageAgeTeam2;
        int sum2 = 0;
        for (int i : numberPlayersTeam2) {
            sum2 += i;
        }
        averageAgeTeam2 = sum2 / numberPlayersTeam2.length;

        System.out.println("Team #2" + " - average age players: " + averageAgeTeam2);
    }
}

